import { Component } from "@angular/core";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "input-decorator";
  parentMessage = "message from parent"; // <-- Nuevo atributo
  constructor() {}
}
