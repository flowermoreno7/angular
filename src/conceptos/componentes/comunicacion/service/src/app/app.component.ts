import { Component } from "@angular/core";
import { ServicioService } from "./servicio.service";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {
  title = "service";
  message: string;

  constructor(private servicioService: ServicioService) {}

  getServiceMessage(): void {
    this.message = this.servicioService.serviceMessage;
    console.log("getServiceMessage. this.message:" + this.message);
  }
}
